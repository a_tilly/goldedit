import argparse

from goldparser.goldparser import GoldParser
from view.view import EditorView


def main():
    parser = GoldParser()
    EditorView(parser)


if __name__ == "__main__":
    main()