import codecs

from pokemon.pokemon import Pokemon

NUM_POKEMON = 251
POKEMON_NAME_LENGTH = 10
POKEMON_DATA_SIZE = 32
POKEMON_DATA_OFFSET = 0x051B0B
POKEMON_NAMES_OFFSET = 0x1B0B74

# TODO extend table
CHAR_TABLE = {
    0x00: "?",

    0x80: "A",
    0x81: "B",
    0x82: "C",
    0x83: "D",
    0x84: "E",
    0x85: "F",
    0x86: "G",
    0x87: "H",
    0x88: "I",
    0x89: "J",
    0x8A: "K",
    0x8B: "L",
    0x8C: "M",
    0x8D: "N",
    0x8E: "O",
    0x8F: "P",
    0x90: "Q",
    0x91: "R",
    0x92: "S",
    0x93: "T",
    0x94: "U",
    0x95: "V",
    0x96: "W",
    0x97: "X",
    0x98: "Y",
    0x99: "Z",
    
    0xef: "♂",
    0xf5: "♀"
    }

class GoldParser():
    def __init__(self):
        self.pokemon_list = []
        self.pokemon_map = {}
        self.pre_bytes = b''
        self.post_bytes = b''
        
    def parse_rom(self, path):
        with codecs.open(path, "rb") as f:
            i = 0
            j = 0

            self.pre_bytes = f.read(POKEMON_DATA_OFFSET)

            # Read pokemon names
            pokemon_names = []
            f.seek(POKEMON_NAMES_OFFSET)
            while j < NUM_POKEMON:
                name = ""
                while i < POKEMON_NAME_LENGTH:
                    try:
                        byte = f.read(1)
                        value = ord(byte)
                        name += CHAR_TABLE.get(value, "")
                    except Exception as e:
                        raise Exception("Error reading pokemon name byte number %d: %s" % (i, str(e)))
                    i += 1
                pokemon_names.append(name)
                j += 1
                i = 0
        
            # Read pokemon data
            j = 0
            f.seek(POKEMON_DATA_OFFSET)
            while j < NUM_POKEMON:
                values = []
                while i < POKEMON_DATA_SIZE:
                    try:
                        byte = f.read(1)
                        value = ord(byte)
                        values.append(value)
                    except Exception as e:
                        raise Exception("Error reading pokemon data byte number %d: %s" % (i, str(e)))
        
                    i += 1
                try:
                    name = pokemon_names[values[0] - 1]
                except IndexError:
                    raise Exception("Failed to find name for pokemon %d" % j)
         
                pokemon = Pokemon(
                    name=name,
                    index=values[0],
                    hp=values[1],
                    attack=values[2],
                    defense=values[3],
                    speed=values[4],
                    sp_attack=values[5],
                    sp_defense=values[6],
                    type1=values[7],
                    type2=values[8],
                    catch_rate=values[9],
                    base_exp=values[10],
                    held_item_1=values[11],
                    held_item_2=values[12],
                    gender_ratio=values[13],
                    unknown1=values[14],
                    egg_cycles=values[15],
                    unknown2=values[16],
                    dimensions=values[17],
                    unknown3=values[18],
                    unknown4=values[19],
                    unknown5=values[20],
                    unknown6=values[21],
                    growth_rate=values[22],
                    egg_groups=values[23],
                    tm_flags=values[24:],
                )
        
                self.pokemon_list.append(pokemon)
                self.pokemon_map[name] = pokemon

                j += 1
                i = 0

            self.post_bytes = f.read()
            
    def write_to_rom(self, path):
        new_bytes = b''

        for pokemon in self.pokemon_list:
            new_bytes += pokemon.get_bytes()

        new_file = self.pre_bytes + new_bytes + self.post_bytes

        with codecs.open(path, "wb") as f:
            f.write(new_file)