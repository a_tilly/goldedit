value_order = ["hp", "attack", "defense", "speed", "sp_attack", "sp_defense", "type1" ,"type2" ,"catch_rate",
               "base_exp", "held_item_1", "held_item_2", "gender_ratio", "unknown1", "egg_cycles", "unknown2",
               "dimensions", "unknown3", "unknown4", "unknown5", "unknown6", "growth_rate", "egg_groups"]

class Pokemon(object):

    def __init__(self, name, index, hp, attack, defense, speed, sp_attack, sp_defense, type1, type2,
                 catch_rate, base_exp, held_item_1, held_item_2, gender_ratio, unknown1, egg_cycles,
                 unknown2, dimensions, unknown3, unknown4, unknown5, unknown6, growth_rate, egg_groups, tm_flags):
        self.value_map = {}

        self.name = name
        self.index = index

        self.value_map["hp"] = hp
        self.value_map["attack"] = attack
        self.value_map["defense"] = defense
        self.value_map["speed"] = speed
        self.value_map["sp_attack"] = sp_attack
        self.value_map["sp_defense"] = sp_defense
        self.value_map["type1"] = type1
        self.value_map["type2"] = type2
        self.value_map["catch_rate"] = catch_rate
        self.value_map["base_exp"] = base_exp
        self.value_map["held_item_1"] = held_item_1
        self.value_map["held_item_2"] = held_item_2
        self.value_map["gender_ratio"] = gender_ratio
        self.value_map["unknown1"] = unknown1
        self.value_map["egg_cycles"] = egg_cycles
        self.value_map["unknown2"] = unknown2
        self.value_map["dimensions"] = dimensions
        self.value_map["unknown3"] = unknown3
        self.value_map["unknown4"] = unknown4
        self.value_map["unknown5"] = unknown5
        self.value_map["unknown6"] = unknown6
        self.value_map["growth_rate"] = growth_rate
        self.value_map["egg_groups"] = egg_groups

        self.tm_flags = tm_flags


    def get_bytes(self):
        values = [self.index]
        for key in value_order:
            values.append(self.value_map[key])

        values.extend(self.tm_flags)
        return bytes(values)