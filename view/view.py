import tkinter
import tkinter.ttk as ttk
from tkinter import filedialog

class EditorView():
    def __init__(self, parser):
        self.parser = parser
        self.entry_map = {}
        self.path = ""

        root = tkinter.Tk()

        menubar = tkinter.Menu(root)

        filemenu = tkinter.Menu(menubar, tearoff=0)
        filemenu.add_command(label="Open", command=self.open)
        filemenu.add_command(label="Save", command=self.save)

        menubar.add_cascade(label="File", menu=filemenu)
        root.config(menu=menubar)

        main_frame = tkinter.Frame(root)

        tree_frame = tkinter.Frame(main_frame)

        self.tree = ttk.Treeview(tree_frame)
        ysb = ttk.Scrollbar(tree_frame, orient="vertical", command=self.tree.yview)
        xsb = ttk.Scrollbar(tree_frame, orient="horizontal", command=self.tree.xview)
        self.tree.configure(yscroll=ysb.set, xscroll=xsb.set)

        self.tree.heading("#0", text="Pokemon")
    
        self.tree.grid(sticky="ns")
        ysb.grid(row=0, column=1, sticky="ns")
        xsb.grid(row=1, column=0, sticky="ew")
        
        self.tree.bind("<<TreeviewSelect>>", self.select_item)
        tree_frame.grid(sticky="nw")

        self.left_data_frame = tkinter.Frame(tree_frame)
        self.right_data_frame = tkinter.Frame(tree_frame)
        self.left_data_label = tkinter.Label(self.left_data_frame, text="Pokemon basic stats and data")
        self.left_data_label.grid()
        self.right_data_label = tkinter.Label(self.right_data_frame)
        self.right_data_label.grid()

        # TODO add support for editing TM flags

        self.left_data_frame.grid(row=0, column=2, sticky="ne")
        self.right_data_frame.grid(row=0, column=3, sticky="ne")

        main_frame.grid()
        root.mainloop()

    def populate(self, path):
        self.parser.parse_rom(path)
        value_map = self.parser.pokemon_list[0].value_map
        count = 0

        for key, value in value_map.items():
            data_frame = self.left_data_frame if count % 2 == 0 else self.right_data_frame
            label = tkinter.Label(data_frame, text=key)
            label.grid()

            entry = tkinter.Entry(data_frame)
            entry.insert(0, value)
            entry.grid()

            self.entry_map[key] = entry
            count += 1
        
        for node in self.parser.pokemon_list:
            self.tree.insert("", "end", text=node.name)
        
    def select_item(self, *args):
        pokemon = self.get_current_pokemon()

        for key, value in pokemon.value_map.items():
            self.entry_map[key].delete(0, tkinter.END)
            self.entry_map[key].insert(0, value)

    def get_current_pokemon(self):
        item = self.tree.focus()
        text = self.tree.item(item, "text")
        return self.parser.pokemon_map.get(text)

    def open(self):
        self.path = filedialog.askopenfilename()
        self.populate(self.path)

    def save(self):
        if self.path:
            pokemon = self.get_current_pokemon()
            for key, value in self.entry_map.items():
                pokemon.value_map[key] = int(value.get())
            
            self.parser.write_to_rom(self.path)